# MODO DE USO:


## Ver alérgenos de una comida:

Petición:
```
$ curl http://localhost:8080/api/v1/meals/{id}/allergens
```

Respuesta:
```
[
    {
        "allergen_id": "2",
        "name": "leche"
    },
    {
        "allergen_id": "5",
        "name": "huevo"
    },
]
```


## Ver comidas con cierto alérgeno:

Petición:
```
$ curl http://localhost:8080/api/v1/allergens/{id}/meals
```

Respuesta:
```
[
    {
        "meal_id": "2",
        "name": "macarrones"
    },
    {
        "meal_id": "7",
        "name": "lasaña"
    }
]
```


## Crear comida:

Petición:
```
$ curl -X POST http://localhost:8080/api/v1/meals
```

Body:
```
    {
        "name": "paella"
    }
```

## Crear alérgeno:

Petición:
```
$ curl -X POST http://localhost:8080/api/v1/allergens
```

Body:
```
    {
        "name": "soja"
    }
```

## Crear ingrediente:

Petición:
```
$ curl -X POST http://localhost:8080/api/v1/ingredients
```

Body:
```
    {
        "name": "aceite de soja"
    }
```


## Actualizar comida:

Petición:
```
$ curl -X PUT http://localhost:8080/api/v1/users/6
```
Body:
```
{
    "name": "sopa de marisco",

}
```

Respuesta:
```
{
    "meal_id": "11",
    "name": "sopa de marisco",
    "last_update": "2018-05-30 11:09:27"
}
```


## Ver ayuda:

Petición:
```
$ curl http://localhost:8080
```

Respuesta:
```
{
    "tasks": "http:\/\/localhost:8080\/api\/v1\/tasks",
    "users": "http:\/\/localhost:8080\/api\/v1\/users",
    "status": "http:\/\/localhost:8080\/status",
    "version": "http:\/\/localhost:8080\/version",
    "this help": "http:\/\/localhost:8080\/"
}
```


## Ver version:

Petición:
```
$ curl http://localhost:8080/version
```

Respuesta:
```
{
    "version": "1.0"
}
```


## Ver status:

Petición:
```
$ curl http://localhost:8080/status
```

Respuesta:
```
{
    "status": "OK"
}
```


