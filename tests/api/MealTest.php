<?php

namespace Tests\api;

class MealTest extends BaseTestCase
{
    private static $id;

    /**
     * Test Get Meals By Allergen.
     */
    public function testGetMealsByAllergen()
    {
        $response = $this->runApp('GET', '/api/v1/allergens/3/meals');
        
        $result = (string) $response->getBody();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('meal_id', $result);
        $this->assertContains('name', $result);
        $this->assertNotContains('error', $result);
    }

    /**
     * Test Create Meal.
     */
    public function testCreateMeal()
    {
        $response = $this->runApp(
            'POST', '/api/v1/meals', ['name' => 'nuevacomida']
        );

        $result = (string) $response->getBody();
        self::$id = json_decode($result)->meal_id;
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('meal_id', $result);
        $this->assertContains('name', $result);
        $this->assertContains('nuevacomida', $result);
        $this->assertNotContains('error', $result);
    }

    /**
     * Test Create Meal Without Name.
     */
    public function testCreateMealWithOutMealName()
    {
        $response = $this->runApp('POST', '/api/v1/meals');

        $result = (string) $response->getBody();
        
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertNotContains('meal_id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }

    /**
     * Test Create Meal With Invalid MealName.
     */
    public function testCreateMealWithInvalidMealName()
    {
        $response = $this->runApp(
            'POST', '/api/v1/meals', ['name' => 'z']
        );

        $result = (string) $response->getBody();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertNotContains('meal_id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }

    /**
     * Test Update Meal.
     */
    public function testUpdateMeal()
    {
        $response = $this->runApp(
            'PUT', '/api/v1/meals/' . self::$id, ['name' => 'actualizarcomida']
        );

        $result = (string) $response->getBody();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('meal_id', $result);
        $this->assertContains('name', $result);
        $this->assertNotContains('error', $result);
    }

    /**
     * Test Update Meal Without Send Data.
     */
    public function testUpdateMealWithOutSendData()
    {
        $response = $this->runApp('PUT', '/api/v1/meals/' . self::$id);

        $result = (string) $response->getBody();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertNotContains('meal_id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }

    /**
     * Test Update Meal Not Found.
     */
    public function testUpdateMealNotFound()
    {
        $response = $this->runApp(
            'PUT', '/api/v1/meals/123456789', ['name' => 'actualizarcomida']
        );

        $result = (string) $response->getBody();

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertNotContains('meal_id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }
}
