<?php

namespace Tests\api;

class AllergenTest extends BaseTestCase
{
    private static $id;

    /**
     * Test Create Allergen.
     */
    public function testCreateAllergen()
    {
        $response = $this->runApp(
            'POST', '/api/v1/allergens', ['name' => 'nuevo alergeno']
        );

        $result = (string) $response->getBody();

        self::$id = json_decode($result)->allergen_id;

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('allergen_id', $result);
        $this->assertContains('name', $result);
        $this->assertContains('nuevo alergeno', $result);
        $this->assertNotContains('error', $result);
    }

    /**
     * Test Create Allergen Without Name.
     */
    public function testCreateAllergenWithoutName()
    {
        $response = $this->runApp('POST', '/api/v1/allergens');

        $result = (string) $response->getBody();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertNotContains('allergen_id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }

    /**
     * Test Create Allergen With Invalid Name.
     */
    public function testCreateAllergenWithInvalidName()
    {
        $response = $this->runApp(
            'POST', '/api/v1/allergens',
            ['name' => 'z', 'email' => 'email@example.com']
        );

        $result = (string) $response->getBody();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertNotContains('allergen_id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }
}
