<?php

namespace Tests\api;

class IngredientTest extends BaseTestCase
{
    private static $id;

    /**
     * Test Get Allergens of a Meal.
     */
    public function testGetAllergensByMeal()
    {
        $response = $this->runApp('GET', '/api/v1/meals/2/allergens');

        $result = (string) $response->getBody();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('allergen_id', $result);
        $this->assertContains('name', $result);
        $this->assertNotContains('error', $result);
    }

    /**
     * Test Create Ingredient.
     */
    public function testCreateIngredient()
    {
        $response = $this->runApp(
            'POST', '/api/v1/ingredients', ['name' => 'nuevo ingrediente']
        );

        $result = (string) $response->getBody();

        self::$id = json_decode($result)->ingredient_id;

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('ingredient_id', $result);
        $this->assertContains('name', $result);
        $this->assertContains('nuevo ingrediente', $result);
        $this->assertNotContains('error', $result);
    }

    /**
     * Test Create Ingredient Without Name.
     */
    public function testCreateIngredientWithoutName()
    {
        $response = $this->runApp('POST', '/api/v1/ingredients');

        $result = (string) $response->getBody();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertNotContains('id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }

    /**
     * Test Create Ingredient With Invalid Name.
     */
    public function testCreateIngredientWithInvalidName()
    {
        $response = $this->runApp(
            'POST', '/api/v1/ingredients', ['name' => 'z']
        );

        $result = (string) $response->getBody();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertNotContains('ingredient_id', $result);
        $this->assertNotContains('name', $result);
        $this->assertContains('error', $result);
    }
}
