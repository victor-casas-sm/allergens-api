<?php

use App\Service\MealService;
use App\Service\AllergenService;
use App\Service\IngredientService;
use App\Repository\MealRepository;
use App\Repository\AllergenRepository;
use App\Repository\IngredientRepository;
use App\Handlers\ApiError;

$container = $app->getContainer();

/**
 * @return ApiError
 */
$container["errorHandler"] = function() {
    return new ApiError;
};

/**
 * @param ContainerInterface $container
 * @return MealService
 */
$container['meal_service'] = function($container) {
    return new MealService($container->get('meal_repository'));
};

/**
 * @param ContainerInterface $container
 * @return MealRepository
 */
$container['meal_repository'] = function($container) {
    return new MealRepository($container->get('db'));
};

/**
 * @param ContainerInterface $container
 * @return AllergenService
 */
$container['allergen_service'] = function($container) {
    return new AllergenService($container->get('allergen_repository'));
};

/**
 * @param ContainerInterface $container
 * @return AllergenRepository
 */
$container['allergen_repository'] = function($container) {
    return new AllergenRepository($container->get('db'));
};

/**
 * @param ContainerInterface $container
 * @return IngredientService
 */
$container['ingredient_service'] = function($container) {
    return new IngredientService($container->get('ingredient_repository'));
};

/**
 * @param ContainerInterface $container
 * @return IngredientRepository
 */
$container['ingredient_repository'] = function($container) {
    return new IngredientRepository($container->get('db'));
};
