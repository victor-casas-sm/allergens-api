SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS allergens_api;
CREATE SCHEMA allergens_api;
USE allergens_api;

--
-- Table structure for table `allergen`
--

CREATE TABLE allergen (
  allergen_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (allergen_id),
  KEY idx_allergen_name (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of allergen
-- ----------------------------
INSERT INTO `allergen` (`name`) VALUES ('huevo');
INSERT INTO `allergen` (`name`) VALUES ('trigo');
INSERT INTO `allergen` (`name`) VALUES ('leche');
INSERT INTO `allergen` (`name`) VALUES ('crustaceo');

--
-- Table structure for table `ingredient`
--

CREATE TABLE ingredient (
  ingredient_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (ingredient_id),
  KEY idx_ingredient_name (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ingredient
-- ----------------------------
INSERT INTO `ingredient` (`name`) VALUES ('harina');
INSERT INTO `ingredient` (`name`) VALUES ('tomate');
INSERT INTO `ingredient` (`name`) VALUES ('marisco');
INSERT INTO `ingredient` (`name`) VALUES ('arroz');
INSERT INTO `ingredient` (`name`) VALUES ('nata');
INSERT INTO `ingredient` (`name`) VALUES ('sal');


--
-- Table structure for table `meal`
--

CREATE TABLE meal (
  meal_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (meal_id),
  KEY idx_meal_name (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of meal
-- ----------------------------
INSERT INTO `meal` (`name`) VALUES ('paella');
INSERT INTO `meal` (`name`) VALUES ('macarrones');
INSERT INTO `meal` (`name`) VALUES ('pollo');
INSERT INTO `meal` (`name`) VALUES ('ensalada');
INSERT INTO `meal` (`name`) VALUES ('pizza');

--
-- Table structure for table `ingredient_allergen`
--

CREATE TABLE ingredient_allergen (
  allergen_id SMALLINT UNSIGNED NOT NULL,
  ingredient_id SMALLINT UNSIGNED NOT NULL,
  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (allergen_id, ingredient_id),
  KEY idx_fk_ingredient_id (`ingredient_id`),
  CONSTRAINT fk_ingredient_allergen_allergen FOREIGN KEY (allergen_id) REFERENCES allergen (allergen_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT fk_ingredient_allergen_ingredient FOREIGN KEY (ingredient_id) REFERENCES ingredient (ingredient_id) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ingredient_allergen
-- ----------------------------
INSERT INTO `ingredient_allergen` (`allergen_id`, `ingredient_id`) VALUES (1, 6);
INSERT INTO `ingredient_allergen` (`allergen_id`, `ingredient_id`) VALUES (2, 1);
INSERT INTO `ingredient_allergen` (`allergen_id`, `ingredient_id`) VALUES (3, 5);
INSERT INTO `ingredient_allergen` (`allergen_id`, `ingredient_id`) VALUES (4, 6);

--
-- Table structure for table `meal_ingredient`
--

CREATE TABLE meal_ingredient (
  ingredient_id SMALLINT UNSIGNED NOT NULL,
  meal_id SMALLINT UNSIGNED NOT NULL,
  last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (ingredient_id, meal_id),
  KEY idx_fk_meal_id (`meal_id`),
  CONSTRAINT fk_meal_ingredient_ingredient FOREIGN KEY (ingredient_id) REFERENCES ingredient (ingredient_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fk_meal_ingredient_meal FOREIGN KEY (meal_id) REFERENCES meal (meal_id) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of meal_ingredient
-- ----------------------------
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (1, 1);
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (2, 1);
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (2, 3);
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (2, 4);
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (4, 1);
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (5, 2);
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (6, 1);
INSERT INTO `meal_ingredient` (`ingredient_id`, `meal_id`) VALUES (6, 4);

--
-- Triggers for update field last_update` of table `meal` when `meal_ingredient` table changes
--
DELIMITER ///

CREATE TRIGGER update_meal_date_if_inserted_ingredients_trig AFTER INSERT ON meal_ingredient
    FOR EACH ROW
    BEGIN
            UPDATE meal m SET last_update = CURRENT_TIMESTAMP WHERE m.meal_id = NEW.meal_id;
    END;
///

DELIMITER ;

DELIMITER ///

CREATE TRIGGER update_meal_date_if_deleted_ingredients_trig AFTER DELETE ON meal_ingredient
    FOR EACH ROW
    BEGIN
            UPDATE meal m SET last_update = CURRENT_TIMESTAMP WHERE m.meal_id = OLD.meal_id;
    END;
///

DELIMITER ;

DELIMITER ///

CREATE TRIGGER update_meal_date_if_updated_ingredients_trig AFTER UPDATE ON meal_ingredient
    FOR EACH ROW
    BEGIN
            UPDATE meal m SET last_update = CURRENT_TIMESTAMP WHERE m.meal_id = NEW.meal_id;
    END;
///

DELIMITER ;
