<?php

$app->get('/', 'App\Controller\DefaultController:getHelp');
$app->get('/version', 'App\Controller\DefaultController:getVersion');
$app->get('/status', 'App\Controller\DefaultController:getStatus');

$app->group('/api/v1', function () use ($app) {
    $app->group('/meals', function () use ($app) {
        $app->get('/{id}/allergens', 'App\Controller\Allergen\GetAllergensByMeal');
        $app->post('', 'App\Controller\Meal\CreateMeal');
        $app->put('/{id}', 'App\Controller\Meal\UpdateMeal');
    });
    $app->group('/allergens', function () use ($app) {
        $app->get('/{id}/meals', 'App\Controller\Meal\GetMealsByAllergen');
        $app->post('', 'App\Controller\Allergen\CreateAllergen');
    });
    $app->group('/ingredients', function () use ($app) {
        $app->post('', 'App\Controller\Ingredient\CreateIngredient');
    });
});
