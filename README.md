# API REST ALLERGENS-API

Un API REST con microframework Slim PHP.
Se ha desarrollado sobre un IDE en la Nube (Cloud9, en https://c9.io/) por
avería técnica de mi equipo local. En ese entorno se ha actualizado la versión
de php de la 5.9 a la 7.1 para su uso con PHPunit 7, logrando un entorno
moderno y estable. Asimismo se usa composer como gestor de dependencias.

Deseché la idea de implementar una solución con php nativo, y preferí usar
Slim Framework, por estar especialmente diseñado para el desarrollo rápido
de aplicaciopnes REST, y usar estándares y tecnologías bien conocidas en el
ecosistema php.

## INSTALACIÓN:

### 1- Descargar el proyecto:

```bash
$ cd path-to-your-projects
$ git clone https://gitlab.com/victor-casas-sm/allergens-api.git
$ cd allergens-api
$ composer install
```


### 2- Crear nueva base de datos MySQL: "allergens_api".

Desde la línea de comandos ejecutar:

```bash
mysql -e 'CREATE DATABASE allergens_api;'
```


### 3- Crear la estructura y cargar datos de prueba en la base de datos.

La base de datos se puede actualizar manualmente utilizando el siguiente archivo: [schema.sql](app/data/schema.sql).

También se puede ejecutar desde la línea de comandos:

```
mysql allergens_api < app/data/schema.sql
```


### 4- Configurar los datos de acceso a MySQL.

Editar archivo de configuración: `app/settings.php`

```php
// Database connection settings
'db' => [
    'host' => '127.0.0.1',
    'dbname' => 'allergens_api',
    'user' => 'YourMysqlUser',
    'pass' => 'YourMysqlPass',
],
```


## SERVIDOR LOCAL:

Se puede iniciar el servidor web interno de PHP ejecutando:

```bash
$ composer start
```


### NOTA:

Se puede acceder localmente al proyecto ingresando a: 
[Ayuda](http://localhost:8080), 
[Alérgenos de una comida](http://localhost:8080/api/v1/meals/{id}/allergens), 
[Comidas con cierto alérgeno](http://localhost:8080/api/v1/allergens/{id}/meals).

El comando `composer start` sería el equivalente a ejecutar:

```bash
$ php -S 0.0.0.0:8080 -t public index.php
```


## TESTS:

Acceder a la ruta del proyecto y ejecutar los tests con `phpunit`:

```bash
PHPUnit 6.1.3 by Sebastian Bergmann and contributors.

................................                                  32 / 32 (100%)

Time: 212 ms, Memory: 8.00MB

OK (32 tests, 138 assertions)
```


## DOCUMENTACIÓN:

### AYUDA Y MANUAL DE USO:

Para más información sobre el modo de uso de la API REST, ver el siguiente documento: [Manual de Uso](DOC.md).


### IMPORTAR EN POSTMAN:

Toda la información de la API, preparada para descargar y utilizar como colección de postman... muy pronto :-) en
[Importar Colección](https://www.getpostman.com/collections/{id}).
