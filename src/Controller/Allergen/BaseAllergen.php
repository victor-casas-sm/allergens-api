<?php

namespace App\Controller\Allergen;

use App\Controller\BaseController;
use App\Service\AllergenService;
use Slim\Container;

/**
 * Base Allergen Controller.
 */
abstract class BaseAllergen extends BaseController
{
    /**
     * @var AllergenService
     */
    protected $allergenService;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container->get('logger');
        $this->allergenService = $container->get('allergen_service');
    }

    /**
     * @return AllergenService
     */
    protected function getAllergenService()
    {
        return $this->allergenService;
    }

    /**
     * @return array
     */
    protected function getInput()
    {
        return $this->request->getParsedBody();
    }
}
