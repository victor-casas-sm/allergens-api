<?php

namespace App\Controller\Allergen;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Create Allergen Controller.
 */
class CreateAllergen extends BaseAllergen
{
    /**
     * Create a allergen.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $input = $this->getInput();
        $result = $this->getAllergenService()->createAllergen($input);

        return $this->jsonResponse('success', $result, 201);
    }
}
