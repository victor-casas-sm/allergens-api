<?php

namespace App\Controller\Allergen;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Get Allergens by Meal Controller.
 */
class GetAllergensByMeal extends BaseAllergen
{
    /**
     * Get allergens by meal.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $mealId = $this->args['id'];
        $result = $this->getAllergenService()->getAllergensByMeal($mealId);
        
        return $this->jsonResponse('success', $result, 200);
    }
}
