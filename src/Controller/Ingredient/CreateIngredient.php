<?php

namespace App\Controller\Ingredient;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Create Ingredient Controller.
 */
class CreateIngredient extends BaseIngredient
{
    /**
     * Create an ingredient.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $input = $this->getInput();
        $result = $this->getIngredientService()->createIngredient($input);

        return $this->jsonResponse('success', $result, 201);
    }
}
