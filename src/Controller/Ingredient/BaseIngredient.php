<?php

namespace App\Controller\Ingredient;

use App\Controller\BaseController;
use App\Service\IngredientService;
use Slim\Container;

/**
 * Base Ingredient Controller.
 */
abstract class BaseIngredient extends BaseController
{
    /**
     * @var ingredientService
     */
    protected $ingredientService;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container->get('logger');
        $this->ingredientService = $container->get('ingredient_service');
    }

    /**
     * @return IngredientService
     */
    protected function getIngredientService()
    {
        return $this->ingredientService;
    }

    /**
     * @return array
     */
    protected function getInput()
    {
        return $this->request->getParsedBody();
    }
}
