<?php

namespace App\Controller\Meal;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Create Meal Controller.
 */
class CreateMeal extends BaseMeal
{
    /**
     * Create a meal.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $input = $this->getInput();
        $result = $this->getMealService()->createMeal($input);

        return $this->jsonResponse('success', $result, 201);
    }
}
