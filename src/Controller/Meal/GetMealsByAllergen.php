<?php

namespace App\Controller\Meal;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Get Allergens by Meal controller.
 */
class GetMealsByAllergen extends BaseMeal
{
    /**
     * Get allergens by meal.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $allergenId = $this->args['id'];
        $result = $this->getMealService()->getMealsByAllergen($allergenId);

        return $this->jsonResponse('success', $result, 200);
    }
}
