<?php

namespace App\Controller\Meal;

use App\Controller\BaseController;
use App\Service\MealService;
use Slim\Container;

/**
 * Base Meal Controller.
 */
abstract class BaseMeal extends BaseController
{
    /**
     * @var UserService
     */
    protected $mealService;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container->get('logger');
        $this->mealService = $container->get('meal_service');
    }

    /**
     * @return MealService
     */
    protected function getMealService()
    {
        return $this->mealService;
    }

    /**
     * @return array
     */
    protected function getInput()
    {
        return $this->request->getParsedBody();
    }
}
