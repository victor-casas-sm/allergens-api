<?php

namespace App\Controller\Meal;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Update Meal Controller.
 */
class UpdateMeal extends BaseMeal
{
    /**
     * Update a meal.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke($request, $response, $args)
    {
        $this->setParams($request, $response, $args);
        $input = $this->getInput();
        $mealId = $this->args['id'];
        $result = $this->getMealService()->updateMeal($input, $mealId);

        return $this->jsonResponse('success', $result, 200);
    }
}
