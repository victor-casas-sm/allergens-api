<?php

namespace App\Repository;

use App\Exception\MealException;
use App\Repository\Query\MealQuery;
use App\Repository\Query\AllergenQuery;

/**
 * Meals Repository.
 */
class MealRepository extends BaseRepository
{
    /**
     * @param \PDO $database
     */
    public function __construct(\PDO $database)
    {
        $this->database = $database;
    }

    /**
     * Check if the meal exists.
     *
     * @param int $mealId
     * @return object $meal
     * @throws \Exception
     */
    public function checkMeal($mealId)
    {
        $statement = $this->database->prepare(MealQuery::GET_MEAL_QUERY);
        $statement->bindParam('id', $mealId);
        $statement->execute();
        $meal = $statement->fetchObject();
        if (empty($meal)) {
            throw new MealException(MealException::MEAL_NOT_FOUND, 404);
        }

        return $meal;
    }

    /**
     * Get meals by allergen.
     *
     * @return array
     */
    public function getMealsByAllergen($allergenId)
    {
        $statement = $this->database->prepare(AllergenQuery::GET_MEALS_BY_ALLERGEN_QUERY);
        $statement->bindParam('id', $allergenId);
        $statement->execute();

        return $statement->fetchAll();
    }

    /**
     * Create a meal.
     *
     * @param array $data
     * @return object
     * @throws \Exception
     */
    public function createMeal($data)
    {
        $statement = $this->database->prepare(MealQuery::CREATE_MEAL_QUERY);
        $statement->bindParam('name', $data['name']);
        $statement->execute();
        $meal = $this->checkMeal($this->database->lastInsertId());

        return $meal;
    }

    /**
     * Update a meal.
     *
     * @param array $data
     * @param int $mealId
     * @return object
     */
    public function updateMeal($data, $mealId)
    {
        $statement = $this->database->prepare(MealQuery::UPDATE_MEAL_QUERY);
        $statement->bindParam('id', $mealId);
        $statement->bindParam('name', $data['name']);
        $statement->execute();
        $meal = $this->checkMeal($mealId);

        return $meal;
    }
}
