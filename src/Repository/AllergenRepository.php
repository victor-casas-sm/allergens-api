<?php

namespace App\Repository;

use App\Exception\AllergenException;
use App\Repository\Query\AllergenQuery;
use App\Repository\Query\MealQuery;

/**
 * Allergens Repository.
 */
class AllergenRepository extends BaseRepository
{
    /**
     * @param \PDO $database
     */
    public function __construct(\PDO $database)
    {
        $this->database = $database;
    }

    /**
     * Check if the allergen exists.
     *
     * @param int $allergenId
     * @return object $allergen
     * @throws \Exception
     */
    public function checkAllergen($allergenId)
    {
        $statement = $this->database->prepare(AllergenQuery::GET_ALLERGEN_QUERY);
        $statement->bindParam('id', $allergenId);
        $statement->execute();
        $allergen = $statement->fetchObject();
        if (empty($allergen)) {
            throw new AllergenException(AllergenException::ALLERGEN_NOT_FOUND, 404);
        }

        return $allergen;
    }

    /**
     * Get allergens by meal.
     *
     * @return array
     */
    public function getAllergensByMeal($mealId)
    {
        $statement = $this->database->prepare(MealQuery::GET_ALLERGENS_BY_MEAL_QUERY);
        $statement->bindParam('id', $mealId);
        $statement->execute();

        return $statement->fetchAll();
    }

    /**
     * Create a allergen.
     *
     * @param array $data
     * @return object
     * @throws \Exception
     */
    public function createAllergen($data)
    {
        $statement = $this->database->prepare(AllergenQuery::CREATE_ALLERGEN_QUERY);
        $statement->bindParam('name', $data['name']);
        $statement->execute();
        $allergen = $this->checkAllergen($this->database->lastInsertId());

        return $allergen;
    }
}
