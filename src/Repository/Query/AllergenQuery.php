<?php

namespace App\Repository\Query;

/**
 * Allergens Query.
 */
abstract class AllergenQuery
{
    const GET_MEALS_BY_ALLERGEN_QUERY = 'SELECT DISTINCT m.meal_id, m.name
                                            FROM allergen a
                                            LEFT JOIN ingredient_allergen ia on( ia.allergen_id = a.allergen_id ) 
                                            LEFT JOIN meal_ingredient pi on( ia.ingredient_id = pi.ingredient_id )
                                            JOIN ingredient i ON ( pi.ingredient_id = i.ingredient_id ) 
                                            JOIN meal m ON ( pi.meal_id = m.meal_id )
                                            WHERE a.allergen_id = :id';
    const GET_ALLERGEN_QUERY = 'SELECT * FROM allergen WHERE allergen_id = :id';
    const CREATE_ALLERGEN_QUERY = 'INSERT INTO allergen (name) VALUES (:name)';
}
