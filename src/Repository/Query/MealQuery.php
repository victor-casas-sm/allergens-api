<?php

namespace App\Repository\Query;

/**
 * Tasks Query.
 */
abstract class MealQuery
{
    const GET_ALLERGENS_BY_MEAL_QUERY = 'SELECT DISTINCT a.allergen_id, a.name
                                            FROM meal m
                                            JOIN meal_ingredient pi ON ( m.meal_id = pi.meal_id ) 
                                            JOIN ingredient_allergen ia ON ( pi.ingredient_id = ia.ingredient_id ) 
                                            JOIN ingredient i ON ( pi.ingredient_id = i.ingredient_id ) 
                                            JOIN allergen a ON ( ia.allergen_id = a.allergen_id ) 
                                            WHERE m.meal_id = :id';
                                            
    const GET_MEAL_QUERY = 'SELECT * FROM meal WHERE meal_id = :id';
    
    const CREATE_MEAL_QUERY = 'INSERT INTO meal (name) VALUES (:name)';
    
    const UPDATE_MEAL_QUERY = 'UPDATE meal SET name=:name WHERE meal_id=:id';
    
    const UPDATE_MEAL_LAST_UPDATED_FIELD = 'UPDATE meal_ingredient mi, meal m
                                                SET mi.ingredient_id = 3, m.last_update = CURRENT_TIMESTAMP
                                                WHERE mi.meal_id = 1';
}
