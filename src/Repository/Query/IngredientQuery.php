<?php

namespace App\Repository\Query;

/**
 * Ingredients Query.
 */
abstract class IngredientQuery
{

    const GET_INGREDIENT_QUERY = 'SELECT * FROM ingredient WHERE ingredient_id = :id';
    const CREATE_INGREDIENT_QUERY = 'INSERT INTO ingredient (name) VALUES (:name)';

}
