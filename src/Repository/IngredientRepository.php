<?php

namespace App\Repository;

use App\Exception\IngredientException;
use App\Repository\Query\IngredientQuery;

/**
 * Ingredients Repository.
 */
class IngredientRepository extends BaseRepository
{
    /**
     * @param \PDO $database
     */
    public function __construct(\PDO $database)
    {
        $this->database = $database;
    }

    /**
     * Check if the ingredient exists.
     *
     * @param int $ingredientId
     * @return object $ingredient
     * @throws \Exception
     */
    public function checkIngredient($ingredientId)
    {
        $statement = $this->database->prepare(IngredientQuery::GET_INGREDIENT_QUERY);
        $statement->bindParam('id', $ingredientId);
        $statement->execute();
        $ingredient = $statement->fetchObject();
        if (empty($ingredient)) {
            throw new IngredientException(IngredientException::INGREDIENT_NOT_FOUND, 404);
        }

        return $ingredient;
    }

    /**
     * Create a ingredient.
     *
     * @param array $data
     * @return object
     * @throws \Exception
     */
    public function createIngredient($data)
    {
        $statement = $this->database->prepare(IngredientQuery::CREATE_INGREDIENT_QUERY);
        $statement->bindParam('name', $data['name']);
        $statement->execute();
        $ingredient = $this->checkIngredient($this->database->lastInsertId());

        return $ingredient;
    }
}
