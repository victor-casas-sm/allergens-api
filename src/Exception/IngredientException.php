<?php

namespace App\Exception;

/**
 * Ingredient Exception.
 */
class IngredientException extends BaseException
{
    const INGREDIENT_NOT_FOUND = 'El ingrediente solicitado no existe.';
    const INGREDIENT_NAME_NOT_FOUND = 'No se encontraron ingredientes con ese nombre.';
    const INGREDIENT_NAME_REQUIRED = 'Ingrese el nombre del ingrediente.';
    const INGREDIENT_INFO_REQUIRED = 'Ingrese los datos a actualizar del ingrediente.';
    const INGREDIENT_NAME_INVALID = 'El nombre ingresado es incorrecto.';
    const INGREDIENT_EMAIL_INVALID = 'El email ingresado es incorrecto.';

    /**
     * @param string $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = '', $code = null, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
