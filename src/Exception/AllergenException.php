<?php

namespace App\Exception;

/**
 * Allergen Exception.
 */
class AllergenException extends BaseException
{
    const ALLERGEN_NOT_FOUND = 'El alergeno solicitado no existe.';
    const ALLERGEN_NAME_NOT_FOUND = 'No se encontraron alergenos con ese nombre.';
    const ALLERGEN_NAME_REQUIRED = 'Ingrese el nombre del alergeno.';
    const ALLERGEN_INFO_REQUIRED = 'Ingrese los datos a actualizar del alergeno.';
    const ALLERGEN_NAME_INVALID = 'El nombre ingresado es incorrecto.';

    /**
     * @param string $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = '', $code = null, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
