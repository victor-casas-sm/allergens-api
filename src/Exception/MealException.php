<?php

namespace App\Exception;

/**
 * Meal Exception.
 */
class MealException extends BaseException
{
    const MEAL_NOT_FOUND = 'La comida solicitada no existe.';
    const MEAL_NAME_NOT_FOUND = 'No se encontraron comidas con ese nombre.';
    const MEAL_NAME_REQUIRED = 'Ingrese el nombre de la comida.';
    const MEAL_INFO_REQUIRED = 'Ingrese los datos a actualizar de la comida.';
    const MEAL_NAME_INVALID = 'La comida ingresada es incorrecta.';

    /**
     * @param string $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = '', $code = null, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
