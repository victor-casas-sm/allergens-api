<?php

namespace App\Validation;

use App\Exception\MealException;
use Respect\Validation\Validator as v;

/**
 * Base Validation.
 */
abstract class BaseValidation
{
    /**
     * Validate and sanitize a name.
     *
     * @param string $name
     * @return string
     * @throws \Exception
     */
    protected static function validateName($name)
    {
        if (!v::alnum()->length(2, 50)->validate($name)) {
            throw new MealException(MealException::MEAL_NAME_INVALID, 400);
        }

        return $name;
    }
}
