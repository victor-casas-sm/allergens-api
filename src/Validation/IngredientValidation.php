<?php

namespace App\Validation;

use App\Exception\IngredientException;

/**
 * Ingredient Validation.
 */
abstract class IngredientValidation extends BaseValidation
{
    /**
     * Validate and sanitize input data when create new ingredient.
     *
     * @param array|object|null $input
     * @return array
     * @throws \Exception
     */
    public static function validateInputOnCreateIngredient($input)
    {
        if (!isset($input['name'])) {
            throw new IngredientException(IngredientException::INGREDIENT_NAME_REQUIRED, 400);
        }
        $name = self::validateName($input['name']);

        return ['name' => $name];
    }
}
