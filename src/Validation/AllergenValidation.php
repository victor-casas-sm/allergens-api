<?php

namespace App\Validation;

use App\Exception\AllergenException;

/**
 * Allergen Validation.
 */
abstract class AllergenValidation extends BaseValidation
{
    /**
     * Validate and sanitize input data when create new allergen.
     *
     * @param array|object|null $input
     * @return array
     * @throws \Exception
     */
    public static function validateInputOnCreateAllergen($input)
    {
        if (!isset($input['name'])) {
            throw new AllergenException(AllergenException::ALLERGEN_NAME_REQUIRED, 400);
        }
        $name = self::validateName($input['name']);

        return ['name' => $name];
    }
}
