<?php

namespace App\Validation;

use App\Exception\MealException;
use Respect\Validation\Validator as v;

/**
 * Meal Validation.
 */
abstract class MealValidation extends BaseValidation
{
    /**
     * Validate and sanitize input data when create a new meal.
     *
     * @param array|object|null $input
     * @return array
     * @throws \Exception
     */
    public static function validateInputOnCreateMeal($input)
    {
        if (!isset($input['name'])) {
            throw new MealException(MealException::MEAL_NAME_REQUIRED, 400);
        }
        if (!v::alnum()->length(2, 255)->validate($input['name'])) {
            throw new MealException(MealException::MEAL_NAME_INVALID, 400);
        }
        $name = $input['name'];
        return ['name' => $name];
    }
}
