<?php

namespace App\Service;

use App\Repository\IngredientRepository;
use App\Validation\IngredientValidation as vs;

/**
 * Ingredients Service.
 */
class IngredientService extends BaseService
{
    /**
     * @var IngredientRepository
     */
    protected $ingredientRepository;

    /**
     * @param IngredientRepository $ingredientRepository
     */
    public function __construct(IngredientRepository $ingredientRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
    }

    /**
     * Get ingredients by meal.
     *
     * @return array
     */
    public function getIngredientsByMeal($mealId)
    {
        $ingredients = $this->ingredientRepository->getIngredientsByMeal($mealId);

        return $ingredients;
    }

    /**
     * Create a ingredient.
     *
     * @param array|object|null $input
     * @return object
     */
    public function createIngredient($input)
    {
        $data = vs::validateInputOnCreateIngredient($input);
        $ingredient = $this->ingredientRepository->createIngredient($data);

        return $ingredient;
    }
}
