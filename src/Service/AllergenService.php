<?php

namespace App\Service;

use App\Repository\AllergenRepository;
use App\Validation\AllergenValidation as vs;

/**
 * Allergens Service.
 */
class AllergenService extends BaseService
{
    /**
     * @var AllergenRepository
     */
    protected $allergenRepository;

    /**
     * @param AllergenRepository $allergenRepository
     */
    public function __construct(AllergenRepository $allergenRepository)
    {
        $this->allergenRepository = $allergenRepository;
    }

    /**
     * Get allergens by meal.
     *
     * @return array
     */
    public function getAllergensByMeal($mealId)
    {
        $allergens = $this->allergenRepository->getAllergensByMeal($mealId);

        return $allergens;
    }

    /**
     * Create a allergen.
     *
     * @param array|object|null $input
     * @return object
     */
    public function createAllergen($input)
    {
        $data = vs::validateInputOnCreateAllergen($input);
        $allergen = $this->allergenRepository->createAllergen($data);

        return $allergen;
    }
}
