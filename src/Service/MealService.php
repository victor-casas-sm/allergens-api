<?php

namespace App\Service;

use App\Repository\MealRepository;
use App\Validation\MealValidation as vs;

/**
 * Meals Service.
 */
class MealService extends BaseService
{
    /**
     * @var MealRepository
     */
    protected $mealRepository;

    /**
     * @param MealRepository $mealRepository
     */
    public function __construct(MealRepository $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    /**
     * Get meals by allergens.
     *
     * @return array
     */
    public function getMealsByAllergen($allergenId)
    {
        $meals = $this->mealRepository->getMealsByAllergen($allergenId);

        return $meals;
    }
    
    /**
     * Create a meal.
     *
     * @param array|object|null $input
     * @return object
     */
    public function createMeal($input)
    {
        $data = vs::validateInputOnCreateMeal($input);
        $meal = $this->mealRepository->createMeal($data);

        return $meal;
    }

     /**
     * Check if the meal exists.
     *
     * @param int $mealId
     * @return object
     */
    protected function checkMeal($mealId)
    {
        $meal = $this->mealRepository->checkMeal($mealId);
        return $meal;
    }
    
    /**
     * Update a meal.
     *
     * @param array|object|null $input
     * @param int $mealId
     * @return object
     */
    public function updateMeal($input, $mealId)
    {
        $checkMeal = $this->checkMeal($mealId);
        $data = vs::validateInputOnCreateMeal($input);
        $meal = $this->mealRepository->updateMeal($data, $mealId);
       
        return $meal;
    }
}
